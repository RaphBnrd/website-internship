"""internshipproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import internshipapp.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', internshipapp.views.home, name='home'),
    path('list/', internshipapp.views.list_data, name='list'),
    path('add_structure/', internshipapp.views.add_structure, name='add_structure'),
    path('add_supervisor/', internshipapp.views.add_supervisor, name='add_supervisor'),
    path('add_internship/', internshipapp.views.add_internship, name='add_internship'),
    path('view_structure/<int:structure_id>/', internshipapp.views.view_structure, name='view_structure'),
    path('view_supervisor/<int:supervisor_id>/', internshipapp.views.view_supervisor, name='view_supervisor'),
    path('view_internship/<int:internship_id>/', internshipapp.views.view_internship, name='view_internship'),
    path('edit_structure/<int:structure_id>/', internshipapp.views.edit_structure, name='edit_structure'),
    path('edit_supervisor/<int:supervisor_id>/', internshipapp.views.edit_supervisor, name='edit_supervisor'),
    path('edit_internship/<int:internship_id>/', internshipapp.views.edit_internship, name='edit_internship'),
    path('delete_structure/<int:structure_id>/', internshipapp.views.delete_structure, name='delete_structure'),
    path('delete_supervisor/<int:supervisor_id>/', internshipapp.views.delete_supervisor, name='delete_supervisor'),
    path('delete_internship/<int:internship_id>/', internshipapp.views.delete_internship, name='delete_internship'),
]
