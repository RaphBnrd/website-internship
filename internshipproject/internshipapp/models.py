from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
import geocoder


mapbox_access_token = 'pk.eyJ1IjoicmFwaGJucmQiLCJhIjoiY2t6b2JxY3kzMDBkaTJ2bGdrZGh0eWw1YiJ9.2NQdKAVg0_LKbqxqwMKAIA'

class Structure(models.Model):

    class StructureType(models.TextChoices):
        REASEARCH = 'RE'
        COMPANY = 'CO'
        ASSOCIATION = 'AS'
        PUBLIC = 'PU'
        START_UP = 'ST'
        OTHER = 'OT'
        UNKNOWN = 'UN'
    
    name = models.fields.CharField(max_length=400)
    location_lat = models.fields.FloatField(
        validators=[MinValueValidator(-90), MaxValueValidator(90)], null=True, blank=True
        )
    location_lon = models.fields.FloatField(
        validators=[MinValueValidator(-180), MaxValueValidator(180)], null=True, blank=True
        )
    address = models.fields.CharField(max_length=200)
    city = models.fields.CharField(max_length=100)
    state = models.fields.CharField(max_length=100)
    webpage = models.fields.URLField(null=True, blank=True)
    type = models.fields.CharField(choices=StructureType.choices, max_length=5,
                                    default='UN', null=True, blank=True)
    contact = models.fields.CharField(max_length=500, null=True, blank=True)
    comment = models.fields.CharField(max_length=1000, null=True, blank=True)

    def save(self, *args, **kwargs):
        g = geocoder.mapbox(self.address, key=mapbox_access_token)
        lat_lon = g.latlng  # (lat, long)
        city_g = g.city
        country_g = g.country
        self.location_lat = lat_lon[0]
        self.location_lon = lat_lon[1]
        self.city = city_g
        self.state = country_g
        return super(Structure, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'

class Supervisor(models.Model):
    name = models.fields.CharField(max_length=200)
    domain = models.fields.CharField(max_length=200, null=True, blank=True)
    webpage = models.fields.URLField(null=True, blank=True)
    contact = models.fields.CharField(max_length=500, null=True, blank=True)
    comment = models.fields.CharField(max_length=1000, null=True, blank=True)

    structure = models.ForeignKey(Structure, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.name}'

class Internship(models.Model):

    class InternshipProgress(models.TextChoices):
        NOT_CONTACTED = '09NC'
        FIRST_CONTACT_SENT = '08FCS'
        NEGATIVE_RESPONSE = '10NEG'
        PREPARE_INTERVIEW = '07PRITW'
        INTERVIEW_DONE = '06ITWD'
        ACCEPTED = '05ACC'
        CHOSEN_INTERNSHIP = '04CHINT'
        ADMINISTRATIVE_PROCEDURE = '03ADMIN'
        WAITING_FOR_BEGINING = '02WAITB'
        INTERNSHIP = '01INT'
        FINISHED_INTERNSHIP = '00FIN'

    title = models.fields.CharField(max_length=500)
    description = models.fields.CharField(max_length=2000, null=True, blank=True)
    webpage = models.fields.URLField(null=True, blank=True)
    contact = models.fields.CharField(max_length=500, null=True, blank=True)
    comment = models.fields.CharField(max_length=1000, null=True, blank=True)
    positive_points = models.fields.CharField(max_length=1000, null=True, blank=True)
    negative_points = models.fields.CharField(max_length=1000, null=True, blank=True)
    progress = models.fields.CharField(choices=InternshipProgress.choices, max_length=15,
                                    default='09NC', null=True, blank=True)
    
    structure = models.ForeignKey(Structure, null=True, on_delete=models.SET_NULL)
    supervisor = models.ForeignKey(Supervisor, null=True, on_delete=models.SET_NULL)

