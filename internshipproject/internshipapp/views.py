from django.shortcuts import render, redirect, get_object_or_404
from numpy import mean

from . import models, forms


def home(request):
    mapbox_access_token = 'pk.eyJ1IjoicmFwaGJucmQiLCJhIjoiY2t6b2JxY3kzMDBkaTJ2bGdrZGh0eWw1YiJ9.2NQdKAVg0_LKbqxqwMKAIA'
    structures = models.Structure.objects.order_by("-location_lat")
    # define initial view of the map
    ini_zoom = 0.001
    mean_lon, mean_lat = 0, 0
    if len(structures)>0:
        loc_lons = [structure.location_lon for structure in structures]
        loc_lats = [structure.location_lat for structure in structures]
        mean_lon = mean(loc_lons)
        mean_lat = mean(loc_lats)
        range_lons = max(loc_lons)- min(loc_lons)
        range_lats = max(loc_lats) - min(loc_lats)
        range_lonlat_lim_zoom = [ [(130,40), 2],
                                [(35,10), 4],
                                [(10,3), 6],
                                [(1,0.5), 8] ]    # limit range lon-lat associated to a zoom
        for ele in range_lonlat_lim_zoom:
            if 2*range_lons<ele[0][0] and 2*range_lats<ele[0][1]:   # 2 factor for security
                ini_zoom = ele[1]
            else:
                break
    return render(request, 'internshipapp/home.html',
                  context={'structures':structures,
                           'mapbox_access_token':mapbox_access_token,
                           'mean_lon':mean_lon,
                           'mean_lat':mean_lat,
                           'ini_zoom':ini_zoom})


def list_data(request):
    structures = models.Structure.objects.all()
    supervisors = models.Supervisor.objects.all()
    internships = models.Internship.objects.order_by("progress")
    return render(request, 'internshipapp/list_data.html',
                  context={'structures':structures,
                           'supervisors':supervisors,
                           'internships':internships})


def add_structure(request):
    structure_form = forms.StructureForm()
    if request.method== 'POST':
        structure_form = forms.StructureForm(request.POST)
        if structure_form.is_valid():
            structure = structure_form.save(commit=False)
            structure.save()
            return redirect('home')
    return render(request, 'internshipapp/add_structure.html',
                  context={'structure_form':structure_form})


def add_supervisor(request):
    supervisor_form = forms.SupervisorForm()
    if request.method== 'POST':
        supervisor_form = forms.SupervisorForm(request.POST)
        if supervisor_form.is_valid():
            supervisor = supervisor_form.save(commit=False)
            supervisor.save()
            return redirect('home')
    return render(request, 'internshipapp/add_supervisor.html',
                  context={'supervisor_form':supervisor_form})


def add_internship(request):
    internship_form = forms.InternshipForm()
    if request.method== 'POST':
        internship_form = forms.InternshipForm(request.POST)
        if internship_form.is_valid():
            internship = internship_form.save(commit=False)
            internship.save()
            return redirect('home')
    return render(request, 'internshipapp/add_internship.html',
                  context={'internship_form':internship_form})

def view_structure(request, structure_id):
    structure_chosen = get_object_or_404(models.Structure, id=structure_id)
    mapbox_access_token = 'pk.eyJ1IjoicmFwaGJucmQiLCJhIjoiY2t6b2JxY3kzMDBkaTJ2bGdrZGh0eWw1YiJ9.2NQdKAVg0_LKbqxqwMKAIA'
    structures = models.Structure.objects.order_by("-location_lat")
    return render(request, 'internshipapp/view_structure.html',
                  context={'structure_chosen':structure_chosen,
                           'structures':structures,
                           'mapbox_access_token':mapbox_access_token})

def view_supervisor(request, supervisor_id):
    supervisor = get_object_or_404(models.Supervisor, id=supervisor_id)
    mapbox_access_token = 'pk.eyJ1IjoicmFwaGJucmQiLCJhIjoiY2t6b2JxY3kzMDBkaTJ2bGdrZGh0eWw1YiJ9.2NQdKAVg0_LKbqxqwMKAIA'
    structures = models.Structure.objects.order_by("-location_lat")
    return render(request, 'internshipapp/view_supervisor.html',
                  context={'supervisor':supervisor,
                           'structures':structures,
                           'mapbox_access_token':mapbox_access_token})

def view_internship(request, internship_id):
    internship = get_object_or_404(models.Internship, id=internship_id)
    mapbox_access_token = 'pk.eyJ1IjoicmFwaGJucmQiLCJhIjoiY2t6b2JxY3kzMDBkaTJ2bGdrZGh0eWw1YiJ9.2NQdKAVg0_LKbqxqwMKAIA'
    structures = models.Structure.objects.order_by("-location_lat")
    return render(request, 'internshipapp/view_internship.html',
                  context={'internship':internship,
                           'structures':structures,
                           'mapbox_access_token':mapbox_access_token})

def edit_structure(request, structure_id):
    structure = get_object_or_404(models.Structure, id=structure_id)
    if request.method == 'POST':
        form = forms.StructureForm(request.POST, instance=structure)
        if form.is_valid():
            form.save()
            return redirect('view_structure', structure_id)
    else:
        form = forms.StructureForm(instance=structure)
    return render(request, 'internshipapp/edit_structure.html',
            context={'structure' : structure, 'form' : form})

def edit_supervisor(request, supervisor_id):
    supervisor = get_object_or_404(models.Supervisor, id=supervisor_id)
    if request.method == 'POST':
        form = forms.SupervisorForm(request.POST, instance=supervisor)
        if form.is_valid():
            form.save()
            return redirect('view_supervisor', supervisor_id)
    else:
        form = forms.SupervisorForm(instance=supervisor)
    return render(request, 'internshipapp/edit_supervisor.html',
            context={'supervisor' : supervisor, 'form' : form})

def edit_internship(request, internship_id):
    internship = get_object_or_404(models.Internship, id=internship_id)
    if request.method == 'POST':
        form = forms.InternshipForm(request.POST, instance=internship)
        if form.is_valid():
            form.save()
            return redirect('view_internship', internship_id)
    else:
        form = forms.InternshipForm(instance=internship)
    return render(request, 'internshipapp/edit_internship.html',
            context={'internship' : internship, 'form' : form})


def delete_structure(request, structure_id):
    structure = models.Structure.objects.get(id=structure_id)
    if request.method == 'POST':
        structure.delete()
        return redirect('home')
    return render(request, 'internshipapp/delete_structure.html',
            context={'structure' : structure})


def delete_supervisor(request, supervisor_id):
    supervisor = models.Supervisor.objects.get(id=supervisor_id)
    if request.method == 'POST':
        supervisor.delete()
        return redirect('home')
    return render(request, 'internshipapp/delete_supervisor.html',
            context={'supervisor' : supervisor})


def delete_internship(request, internship_id):
    internship = models.Internship.objects.get(id=internship_id)
    if request.method == 'POST':
        internship.delete()
        return redirect('home')
    return render(request, 'internshipapp/delete_internship.html',
            context={'internship' : internship})
