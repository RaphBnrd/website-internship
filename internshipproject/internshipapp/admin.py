from django.contrib import admin

from internshipapp.models import Structure, Supervisor, Internship

class StructureAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'type')

class SupervisorAdmin(admin.ModelAdmin):
    list_display = ('name', 'domain')

class InternshipAdmin(admin.ModelAdmin):
    list_display = ('title', 'structure', 'supervisor')

admin.site.register(Structure, StructureAdmin)
admin.site.register(Supervisor, SupervisorAdmin) 
admin.site.register(Internship, InternshipAdmin) 
