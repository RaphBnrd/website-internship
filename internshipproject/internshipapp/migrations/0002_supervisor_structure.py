# Generated by Django 4.0.2 on 2022-02-10 14:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('internshipapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='supervisor',
            name='structure',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='internshipapp.structure'),
        ),
    ]
