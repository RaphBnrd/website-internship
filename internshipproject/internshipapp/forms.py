from django import forms

from . import models

class StructureForm(forms.ModelForm):
    class Meta:
        model = models.Structure
        exclude = ('location_lat', 'location_lon', 'city', 'state' )

class SupervisorForm(forms.ModelForm):
    class Meta:
        model = models.Supervisor
        fields = '__all__'

class InternshipForm(forms.ModelForm):
    class Meta:
        model = models.Internship
        fields = '__all__'
    